<?php

class Plw123ImageResize {

	private $error_level = false;
	private $file = false;
	private $metadata = array();
	
	function image_resize( $file, $max_w, $max_h, $crop = false, $suffix = null, $dest_path = null, $jpeg_quality = 90 ) {
		
		$jpeg_quality = apply_filters( 'jpeg_quality', $jpeg_quality, 'image_resize' );
		$data_array = compact( 'file', 'max_w', 'max_h', 'crop', 'suffix', 'dest_path', 'jpeg_quality' );
		$data = serialize( $data_array );
		$http_args = array(
			'body' => array( 'data' => $data ),
		);
		if( function_exists('plw123dbg_debugfile_write') /*&& PLW123GC_DEBUG==true */ )
			plw123dbg_debugfile_write( $data );

		$data_id = hash_hmac( 'md5', $data, PLW123IMGRS_KEY );
		$imgrs_url = plugins_url( "plw123-irs-resize.php?id=$data_id", __FILE__ );
		$site_url = get_option( 'siteurl' );
		if( false===strpos( $imgrs_url, $site_url ) )
			$imgrs_url = $site_url.$imgrs_url;

		$res = wp_remote_post( $imgrs_url, $http_args );
		if( function_exists('plw123dbg_debugfile_write') /*&& PLW123GC_DEBUG==true */ )
			plw123dbg_debugfile_write( $res );
		if( is_wp_error( $res )  ) {	
			return $res;
		}
		else {
			$response_code =  $res['response']['code'];
			$body = trim( $res['body'] );
			if( '200' == $response_code ) {
				$res_data = @unserialize( $body );
				if( $res_data ) {
					if( true == $res_data[0] )
						$file = $res_data[1];
					else
						return new WP_Error( $res_data[1], __( $res_data[2] ) );
				}
				else {
					return new WP_Error( 'resize_error', $body );			
				}
			}
			else {
				return new WP_Error( 'http_error', "$response_code: $body" );
			}
		}	
		return $file;
	}

	function image_make_intermediate_size( $file, $width, $height, $crop=false ) {
		if( $width || $height ) {
			$resized_file = $this->image_resize($file, $width, $height, $crop);
			if ( !is_wp_error($resized_file) && $resized_file && $info = getimagesize($resized_file) ) {
				$resized_file = apply_filters(	'image_make_intermediate_size', $resized_file	);
				return array(
					'file' => wp_basename( $resized_file ),
					'width' => $info[0],
					'height' => $info[1],
				);
			}
		}
		return false;
	}

	function wrup( $new_path, $path ) {
		$this->file = $path;
		return $new_path;
	}

	function iisa( $sizes ) {
		
		foreach( $sizes as $size => $size_data ) {
			$resized = $this->image_make_intermediate_size( $this->file, $size_data['width'], $size_data['height'], $size_data['crop'] );
			if( $resized )
				$this->metadata['sizes'][$size] = $resized;
		}
		return array();
	}

	function wgam( $metadata, $attachment_id  ) {
		$metadata = array_merge( $metadata, $this->metadata );	
			
		return $metadata;
	}

	static function &instance()
	{
		static $self;

		if( is_null( $self ) )
			$self = new Plw123ImageResize();

		return $self;
	}

	function __construct() {
		add_filter( '_wp_relative_upload_path', array( $this, 'wrup' ), 10, 2 );
		add_filter( 'intermediate_image_sizes_advanced', array( $this, 'iisa' ), 9999 );
		add_filter( 'wp_generate_attachment_metadata', array( $this, 'wgam' ), 10, 2 );
	}
	
}

?>