<?php
/*
Plugin Name: 123 Image Resize
Plugin URI: http://schnurpsel.de/wordpress-plugins/123-image-resize/
Description: Ermöglicht die Nutzung des gesamten PHP-Memory für die Erstellung der WP-Vorschau-Bilder beim Upload.
Author: Ingo Henze
Version: 0.11
Author URI: http://putzlowitsch.de/
*/

function plw123imgrs_activate() {
	if( defined( 'PLW123IMGRS_KEY' ) && ('' == PLW123IMGRS_KEY) ) 
		trigger_error( "Bitte einen eindeutigen Ausdruck 'PLW123IMGRS_KEY' in der Datei plw123-irs-config.php eintragen", E_USER_ERROR );
}

if( function_exists( 'is_admin' ) ) :

	if( is_admin() ) {
		@include( 'plw123-irs-config.php' );
		require_once( 'class-resize.php' );
		
		Plw123ImageResize::instance();

	}
	register_activation_hook( __FILE__, 'plw123imgrs_activate' );

endif; // function_exists( 'is_amin' )

?>